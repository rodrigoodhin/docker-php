FROM ubuntu:20.04
LABEL maintainer="Rodrigo Odhin"

ARG DEBIAN_FRONTEND=noninteractive

# Install libs
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:ondrej/php
RUN apt-get update
RUN apt-get install -y openssh-server vim curl git sudo gpg
RUN apt-get install -y apache2 apache2-utils php8.3 php8.3-cli php8.3-bz2 php8.3-curl php8.3-mbstring php8.3-intl php8.3-sqlite3
RUN apt-get install php8.3-fpm
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN a2enconf php8.3-fpm
RUN a2enmod rewrite
RUN apt-get clean

# install phpmyadmin
RUN apt-get install -y phpmyadmin php-mbstring php-zip php-gd php-json php-curl
RUN phpenmod mbstring
COPY etc/phpmyadmin/phpmyadmin.conf /etc/apache2/sites-available/phpmyadmin.conf
COPY etc/phpmyadmin/config.inc.php /etc/phpmyadmin/config.inc.php 
RUN apt-get clean

# Change PHP config
ENV PHP_UPLOAD_MAX_FILESIZE 5000000000
ENV PHP_POST_MAX_SIZE 100000000000
ENV PHP_MEM_LIMIT 100000000000
RUN echo "post_max_size = 10024M" >> /etc/php/8.3/apache2/php.ini
RUN echo "upload_max_filesize = 5000M" >> /etc/php/8.3/apache2/php.ini
RUN echo "max_file_uploads = 999999" >> /etc/php/8.3/apache2/php.ini
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
COPY etc/ssl/ssl-params.conf /etc/apache2/conf-available/ssl-params.conf
COPY etc/ssl/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf
COPY etc/ssl/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod ssl
RUN a2enmod headers
RUN a2ensite default-ssl
RUN a2enconf ssl-params

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
COPY html /var/www/html

RUN mkdir /var/run/sshd

RUN echo 'root:root' |chpasswd
RUN sed -ri 's/^PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
RUN echo 'Banner /etc/banner' >> /etc/ssh/sshd_config

COPY etc/banner /etc/

RUN useradd -ms /bin/bash app
RUN adduser app sudo
RUN echo 'app:app' |chpasswd

EXPOSE 22
EXPOSE 80
EXPOSE 99
EXPOSE 8000

RUN mkdir /Users

RUN mkdir /scripts
COPY scripts/entrypoint.sh /scripts/entrypoint.sh
RUN ["chmod", "777", "/scripts/entrypoint.sh"]
ENTRYPOINT ["/scripts/entrypoint.sh"]
